# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the fr.romainvigier.MetadataCleaner package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: fr.romainvigier.MetadataCleaner\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-01-19 12:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/fr.romainvigier.MetadataCleaner.desktop.in:6
#: data/fr.romainvigier.MetadataCleaner.desktop.in:7
#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:9
#: metadatacleaner/app.py:43
msgid "Metadata Cleaner"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.desktop.in:8
msgid "Clean metadata from your files"
msgstr ""

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/fr.romainvigier.MetadataCleaner.desktop.in:16
msgid "Metadata;Remover;Cleaner;"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.gschema.xml:10
msgid "Warn before saving cleaned files"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.gschema.xml:11
msgid "Show the warning dialog before saving the cleaned files"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:10
msgid "View and clean metadata in files"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:15
msgid "Romain Vigier"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:22
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when a picture was taken and what camera was used. Office applications "
"automatically add author and company information to documents and "
"spreadsheets. Maybe you don't want to disclose those informations."
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:23
msgid ""
"This tool allows you to view metadata in your files and to get rid of them, "
"as much as possible."
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:49
msgid "New in v1.0.2:"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:51
msgid "Spanish translation (contributed by Óscar Fernández Díaz)"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:52
msgid "Swedish translation (contributed by Åke Engelbrektson)"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:54
msgid "Bug fixes:"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:56
msgid "Files with uppercase extension can now be added"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:62
msgid "New in v1.0.1:"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:64
msgid "German translation (contributed by lux)"
msgstr ""

#. Translators: Add your name here, do not remove previous ones!
#: data/ui/AboutDialog.ui:15
msgid "translator-credits"
msgstr ""

#: data/ui/AboutDialog.ui:24
msgid ""
"<small>This program uses <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</"
"a> to clean the metadata. Show them some love!</small>"
msgstr ""

#: data/ui/AboutMetadataPrivacyDialog.ui:9
msgid "Note about metadata and privacy"
msgstr ""

#: data/ui/AboutMetadataPrivacyDialog.ui:10
msgid ""
"Metadata consist of information that characterizes data. Metadata are used "
"to provide documentation for data products. In essence, metadata answer who, "
"what, when, where, why, and how about every facet of the data that are being "
"documented.\n"
"\n"
"Metadata within a file can tell a lot about you. Cameras record data about "
"when a picture was taken and what camera was used. Office aplications "
"automatically add author and company information to documents and "
"spreadsheets. Maybe you don't want to disclose those informations.\n"
"\n"
"This tool will get rid, as much as possible, of metadata."
msgstr ""

#: data/ui/AboutRemovingMetadataDialog.ui:9
msgid "Note about removing metadata"
msgstr ""

#: data/ui/AboutRemovingMetadataDialog.ui:10
msgid ""
"While this tool is doing its very best to display metadata, it doesn't mean "
"that a file is clean from any metadata if it doesn't show any. There is no "
"reliable way to detect every single possible metadata for complex file "
"formats.\n"
"\n"
"This is why you shouldn't rely on metadata's presence to decide if your file "
"must be cleaned or not."
msgstr ""

#: data/ui/AddFilesButton.ui:9
msgid "Add files"
msgstr ""

#: data/ui/CleanMetadataButton.ui:12
msgid "_Clean"
msgstr ""

#: data/ui/EmptyView.ui:23
msgid "Clean your traces."
msgstr ""

#: data/ui/FileButton.ui:25
msgid "Warning"
msgstr ""

#: data/ui/FileButton.ui:35
msgid "Error"
msgstr ""

#: data/ui/FileButton.ui:45
msgid "Metadata"
msgstr ""

#: data/ui/FileButton.ui:55
msgid "Cleaned"
msgstr ""

#: data/ui/FileRow.ui:14
msgid "Remove file from list"
msgstr ""

#: data/ui/MenuPopover.ui:18
msgid "_New window"
msgstr ""

#: data/ui/MenuPopover.ui:33
msgid "_Lightweight mode"
msgstr ""

#: data/ui/MenuPopover.ui:34
msgid ""
"By default, the removal process might alter a bit the data of your files, in "
"order to remove as much metadata as possible. For example, texts in PDF "
"might not be selectable anymore, compressed images might get compressed "
"again… If you're willing to trade some metadata's presence in exchange of "
"the guarantee that the data of your files won't be modified, the lightweight "
"mode precisely does that."
msgstr ""

#: data/ui/MenuPopover.ui:48
msgid "Note about _metadata and privacy"
msgstr ""

#: data/ui/MenuPopover.ui:55
msgid "Note about _removing metadata"
msgstr ""

#: data/ui/MenuPopover.ui:71
msgid "_Keyboard shortcuts"
msgstr ""

#: data/ui/MenuPopover.ui:78
msgid "_About Metadata Cleaner"
msgstr ""

#: data/ui/MetadataWindow.ui:9
msgid "Metadata details"
msgstr ""

#: data/ui/SaveFilesButton.ui:12
msgid "_Save"
msgstr ""

#: data/ui/SaveWarningDialog.ui:9
msgid "Make sure you backed up your files!"
msgstr ""

#: data/ui/SaveWarningDialog.ui:10
msgid "Once you replace your files, there's no going back."
msgstr ""

#: data/ui/SaveWarningDialog.ui:18
msgid "Don't tell me again"
msgstr ""

#: data/ui/ShortcutsDialog.ui:18
msgctxt "shortcut window"
msgid "Files"
msgstr ""

#: data/ui/ShortcutsDialog.ui:22
msgctxt "shortcut window"
msgid "Add files"
msgstr ""

#: data/ui/ShortcutsDialog.ui:29
msgctxt "shortcut window"
msgid "Clean metadata"
msgstr ""

#: data/ui/ShortcutsDialog.ui:36
msgctxt "shortcut window"
msgid "Save cleaned files"
msgstr ""

#: data/ui/ShortcutsDialog.ui:45
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: data/ui/ShortcutsDialog.ui:49
msgctxt "shortcut window"
msgid "New window"
msgstr ""

#: data/ui/ShortcutsDialog.ui:56
msgctxt "shortcut window"
msgid "Close window"
msgstr ""

#: data/ui/ShortcutsDialog.ui:63
msgctxt "shortcut window"
msgid "Quit"
msgstr ""

#: data/ui/ShortcutsDialog.ui:70
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr ""

#: data/ui/StatusIndicator.ui:35
msgid "Done!"
msgstr ""

#: metadatacleaner/file.py:139
msgid "Something bad happened during the removal, cleaned file not found"
msgstr ""

#: metadatacleaner/filechooserdialog.py:21
msgid "Choose files"
msgstr ""

#: metadatacleaner/filechooserdialog.py:31
msgid "All supported files"
msgstr ""

#: metadatacleaner/filepopover.py:38
msgid "Initializing…"
msgstr ""

#: metadatacleaner/filepopover.py:40
msgid "Error while initializing the file parser."
msgstr ""

#: metadatacleaner/filepopover.py:42
msgid "File type not supported."
msgstr ""

#: metadatacleaner/filepopover.py:43
msgid "File type supported."
msgstr ""

#: metadatacleaner/filepopover.py:44
msgid "Checking metadata…"
msgstr ""

#: metadatacleaner/filepopover.py:46
msgid "Error while checking metadata:"
msgstr ""

#: metadatacleaner/filepopover.py:49
msgid ""
"No metadata have been found.\n"
"The file will be cleaned anyway, better safe than sorry."
msgstr ""

#: metadatacleaner/filepopover.py:52
msgid "Removing metadata…"
msgstr ""

#: metadatacleaner/filepopover.py:54
msgid "Error while removing metadata:"
msgstr ""

#: metadatacleaner/filepopover.py:56
msgid "The file has been cleaned."
msgstr ""

#: metadatacleaner/filepopover.py:57
msgid "Saving the cleaned file…"
msgstr ""

#: metadatacleaner/filepopover.py:58
msgid "Error while saving the file:"
msgstr ""

#: metadatacleaner/filepopover.py:59
msgid "The cleaned file has been saved."
msgstr ""

#: metadatacleaner/metadatadetails.py:39
#, python-brace-format
msgid "{filename}:"
msgstr ""

#: metadatacleaner/statusindicator.py:33
msgid "Processing file {}/{}"
msgstr ""
